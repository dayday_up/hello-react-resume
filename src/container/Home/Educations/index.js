import React from 'react';
import PropTypes from 'prop-types';
import './style.less';

const EducationList = props => {
  const educations = props.educations;
  return (
    <ul className="education-list">
      {educations.map((item, index) => {
        const { year, title, description } = item;
        return (
          <li key={index} className="education-item">
            <h4 className="year">{year}</h4>
            <div className="detail">
              <h5 className="title">{title}</h5>
              <p className="description">{description}</p>
            </div>
          </li>
        );
      })}
    </ul>
  );
};

EducationList.propTypes = {
  educations: PropTypes.array
};

export default EducationList;
