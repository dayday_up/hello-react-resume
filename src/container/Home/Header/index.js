import React from 'react';
import avatar from '../../../assets/avatar.jpg';
import PropTypes from 'prop-types';
import './style.less';

const Header = props => {
  const { name, age } = props.person;
  return (
    <div className="header">
      <img src={avatar} alt="avatar" className="avatar" />
      <h1 className="hello">hello,</h1>
      <h2 className="introduce">{`my name is ${name} ${age} and this is my resume/cv`}</h2>
    </div>
  );
};

Header.propTypes = {
  person: PropTypes.object
};

export default Header;
