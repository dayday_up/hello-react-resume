import React, { Component } from 'react';
import Home from './container/Home';
import './App.less';

class App extends Component {
  render() {
    return (
      <main className="app">
        <Home />
      </main>
    );
  }
}

export default App;
