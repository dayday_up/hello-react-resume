import { get } from '../common/js/fetch';
import Education from './model/Education';
import Person from './model/Person';

export const getInformation = () => {
  const url = 'http://localhost:3000/person';
  return get(url).then(data => {
    let { name, age, description, educations } = data;
    const person = new Person(name, age);
    let educationList = educations.map(item => {
      const { year, title, description } = item;
      return new Education(year, title, description);
    });
    return {
      person: person,
      description: description,
      educations: educationList
    };
  });
};
