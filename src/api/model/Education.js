class Education {
  get year() {
    return this._year;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._description;
  }
  constructor(year, title, description) {
    this._year = year;
    this._title = title;
    this._description = description;
  }
}

export default Education;
